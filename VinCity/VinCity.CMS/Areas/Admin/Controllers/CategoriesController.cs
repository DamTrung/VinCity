﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VinCity.CMS.Areas.Admin.Models;
using VinCity.CMS.Common;
using VinCity.Models.Entities;

namespace VinCity.CMS.Areas.Admin.Controllers
{
    public class CategoriesController : Controller
    {
        private VinCityEntities db = new VinCityEntities();

        // GET: Admin/Categories
        public ActionResult Index()
        {
            var lstData = db.Category.ToList();
            var lstNew = new List<CategoriesViewModel>();
            foreach (Category cate in lstData)
            {
                var cateViewModel = new CategoriesViewModel();
                cateViewModel.categoryInfo = cate;
                cateViewModel.parentCategory = db.Category.Find(cate.ParentId);
                if (cate.Type == 1)
                {
                    cateViewModel.isMenu = true;
                }
                else
                {
                    cateViewModel.isMenu = false;
                }
                lstNew.Add(cateViewModel);
            }
            return View(lstNew);
        }
        [ChildActionOnly]
        public Category getCategory(int cateId)
        {
            Category category = db.Category.Find(cateId);
            return category;
        }
        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category.ToList(), "Id", "Name");
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoriesViewModel category)
        {
            if (ModelState.IsValid)
            {
                category.categoryInfo.CreateAt = DateTime.Now;
                category.categoryInfo.CreateBy = User.Identity.Name;
                if (category.isMenu == true)
                {
                    category.categoryInfo.Type = 1;
                }
                else
                {
                    category.categoryInfo.Type = 0;
                }
                db.Category.Add(category.categoryInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //   ViewBag.ParentId = new SelectList(db.Category.ToList(), "Id", "Name",category.ParentId);
            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.Status = Utitlitys.StatusToList();
            ViewBag.CategoryId = new SelectList(db.Category.ToList(), "Id", "Name");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            CategoriesViewModel viewModel = new CategoriesViewModel();
            viewModel.categoryInfo = category;
            if (viewModel.isMenu == true)
            {
                viewModel.categoryInfo.Type = 1;
            }
            else
            {
                viewModel.categoryInfo.Type = 0;
            }

            return View(viewModel);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoriesViewModel category)
        {
            if (ModelState.IsValid)
            {
                category.categoryInfo.UpdateAt = DateTime.Now;
                category.categoryInfo.UpdateBy = User.Identity.Name;
                db.Entry(category.categoryInfo).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Category.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Category.Find(id);
            db.Category.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
