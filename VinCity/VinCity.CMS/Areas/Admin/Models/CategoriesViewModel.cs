﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VinCity.Models.Entities;

namespace VinCity.CMS.Areas.Admin.Models
{
    public class CategoriesViewModel
    {
        public Category categoryInfo { get; set; }
        public bool isMenu { get; set; }
        public Category parentCategory { get; set; }
    }
}