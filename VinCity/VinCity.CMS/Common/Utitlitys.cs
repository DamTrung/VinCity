﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace VinCity.CMS.Common
{
    public class RolesAttribute : AuthorizeAttribute
    {
        public RolesAttribute(params string[] roles)
        {
            Roles = String.Join(",", roles);
        }
    }
    public static class Utitlitys
    {
        public static string RemoveUnicodeCharactor(string text)
        {
            string result = "";
            try
            {
                for (int i = 33; i < 48; i++)
                {
                    if (i != 44)
                    {
                        text = text.Replace(((char)i).ToString(), "");
                    }
                }
                for (int i = 58; i < 65; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 91; i < 97; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                for (int i = 123; i < 127; i++)
                {
                    text = text.Replace(((char)i).ToString(), "");
                }
                Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
                string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
                result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace("(", "").Replace(")", "").Replace("\"", "").Replace("/", "").Replace("\\", "").Replace("'", "").Replace("“", "").Replace("”", "");
            }
            catch (Exception)
            {

            }
            return result;
        }
        public static SelectList StatusToList()
        {
            var status = new SelectListItem() { Text = "Phê duyệt", Value = ((int)CMS_STATUS.ACTIVE).ToString() };
            var lst = new List<SelectListItem>();
            lst.Add(status);
            status = new SelectListItem() { Text = "Chờ phê duyệt", Value = ((int)CMS_STATUS.PENDING).ToString() };
            lst.Add(status);
            status = new SelectListItem() { Text = "Đã xóa", Value = ((int)CMS_STATUS.DELETED).ToString() };
            lst.Add(status);
            return new SelectList(lst, "Value", "Text");
        }
        public static string ImagePath(string fileName, int type)
        {
            if (type == (int)CMS_IMAGE_TYPE.ARTICLE_TYPE)
            {
                return string.Format("/Uploads/TinTuc/{0}", fileName);
            }
            else
            {
                return string.Format("/Uploads/DuAn/{0}", fileName);
            }
        }
        public static string StatusToString(int status)
        {
            var strResult = string.Empty;
            switch (status)
            {
                case (int)CMS_STATUS.ACTIVE:
                    {
                        strResult = "Đã duyệt";
                        break;
                    }
                case (int)CMS_STATUS.PENDING:
                    {
                        strResult = "Chờ phê duyệt";
                        break;
                    }
                case (int)CMS_STATUS.DELETED:
                    {
                        strResult = "Đã xóa";
                        break;
                    }
                default: break;
            }
            return strResult;
        }

        public static string DatetimeToVN(DateTime dt)
        {
            if (dt == null)
            {
                return "";
            }
            else
            {
                return dt.ToString("dd/MM/yyyy");
            }

        }
    }
    public enum CMS_STATUS
    {
        ACTIVE = 1,
        PENDING = 2,
        DELETED = 3
    }
    public enum CMS_IMAGE_TYPE
    {
        ARTICLE_TYPE = 0,
        PRODUCT_TYPE
    }
}