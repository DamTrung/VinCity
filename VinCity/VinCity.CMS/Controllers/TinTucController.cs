﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VinCity.CMS.Common;
using VinCity.Models.Entities;

namespace VinCity.CMS.Controllers
{
    public class TinTucController : Controller
    {
        private VinCityEntities db = new VinCityEntities();
        // GET: TinTuc
        public ActionResult Index()
        {
            var articles = db.Articles.ToList();
            return View(articles);
        }
        // GET: News/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Article articles = db.Articles.Find(id);
            if (articles == null)
            {
                return HttpNotFound();
            }
            ViewBag.CurrentCateId = articles.Category.Id > 0 ? articles.Category.Id : 0;
            ViewBag.RelatedNews = db.Articles.Where(x => x.CategoryId == articles.CategoryId
                && x.Id != articles.Id && articles.Status == (int)CMS_STATUS.ACTIVE);
            return View(articles);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}