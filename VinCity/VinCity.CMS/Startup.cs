﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VinCity.CMS.Startup))]
namespace VinCity.CMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
