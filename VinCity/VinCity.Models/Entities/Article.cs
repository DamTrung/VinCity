//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VinCity.Models.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Article
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string ThumbURL { get; set; }
        public Nullable<int> Status { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> CreateAt { get; set; }
        public Nullable<System.DateTime> UpdateAt { get; set; }
        public Nullable<bool> HotNew { get; set; }
        public string Title_EN { get; set; }
        public string Summary_EN { get; set; }
        public string Description_EN { get; set; }
    
        public virtual Category Category { get; set; }
    }
}
